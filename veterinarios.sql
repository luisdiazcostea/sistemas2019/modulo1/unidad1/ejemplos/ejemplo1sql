﻿-- Ejemplo de creacion de una base de datos de veterinarios
-- Tiene 3 tablas

DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE IF NOT EXISTS b20190528;
USE b20190528;

/*
  Creando la tabla animales
*/

CREATE TABLE animales(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  raza varchar(100),
  fechaNac date,
  PRIMARY KEY (id)
);

CREATE TABLE veterinarios(
  cod int AUTO_INCREMENT,
  nombre varchar(100),
  especialidad varchar(100),
  PRIMARY KEY (cod) -- Creando la clave
);



CREATE TABLE acuden(
  idanimal int,
  codvet int,
  fecha date,
  PRIMARY KEY (idanimal, codvet, fecha),
  UNIQUE KEY (idanimal),

  CONSTRAINT fkacudenanimal FOREIGN KEY (idanimal) REFERENCES animales (id),
  CONSTRAINT fkacudenveterinarios FOREIGN KEY (codvet) REFERENCES veterinarios (cod)
);

-- Insertar un registro

  INSERT INTO animales (nombre, raza, fechaNac) VALUES 
    ('jorge', 'bulldog', '2000/2/1'),
    ('ana','caniche','2002/1/2');

-- Listar
  SELECT * FROM animales;

  DELETE FROM animales;